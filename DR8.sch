EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:hexagon-robotics
LIBS:w_rtx
LIBS:DR8-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "DR8"
Date "2016-02-14"
Rev "0.1-SNAPSHOT"
Comp "HEXAGON-ROBOTICS"
Comment1 "eMail: hexagon.robotics@gmail.com"
Comment2 "Author: Javier Hernandez Deniz"
Comment3 "Licence:  MIT"
Comment4 ""
$EndDescr
$Comp
L SL869 U2
U 1 1 56C03B28
P 5500 2350
F 0 "U2" H 5500 2450 60  0000 C CNN
F 1 "SL869-DR" H 5500 2250 60  0000 C CNN
F 2 "HEXAGON-ROBOTICS:SL869" H 5500 1150 60  0000 C CNN
F 3 "http://www.telit.com/index.php?eID=tx_nawsecuredl&u=0&g=0&t=1455528650&hash=7b5145fcb51cfc7844cf4f2a5bf23bfbf819c12c&file=downloadZone/Telit_Product-Datasheet_NEW_JUPITER_SL869-DR_01.pdf" H 5500 2350 60  0001 C CNN
	1    5500 2350
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X06 P1
U 1 1 56C03F39
P 10750 1500
F 0 "P1" H 10750 1850 50  0000 C CNN
F 1 "GPS" V 10850 1500 50  0000 C CNN
F 2 "HEXAGON-ROBOTICS:SM06B-GHS-TBT" H 10700 1100 50  0001 C CNN
F 3 "http://www.jst-mfg.com/product/pdf/eng/eGH.pdf" H 10750 1500 50  0001 C CNN
	1    10750 1500
	1    0    0    -1  
$EndComp
$Comp
L ANTENNA ANT1
U 1 1 56C192D2
P 8050 3100
F 0 "ANT1" H 8050 3350 60  0000 C CNN
F 1 "ANTENNA" H 8050 3000 60  0000 C CNN
F 2 "HEXAGON-ROBOTICS:APAE1590R2540AKDB1-T" H 8050 2850 60  0000 C CNN
F 3 "http://www.abracon.com/patchantenna/APAE1590R2540AKDB1-T.pdf" H 8050 3100 60  0001 C CNN
	1    8050 3100
	1    0    0    -1  
$EndComp
Text Notes 7000 3100 0    39   ~ 0
50 Ω
Text GLabel 4250 2050 0    39   Input ~ 0
RX
Text GLabel 4250 2200 0    39   Output ~ 0
TX
Text GLabel 4250 2350 0    39   BiDi ~ 0
SCL
Text GLabel 4250 2500 0    39   BiDi ~ 0
SDA
Text GLabel 7050 2200 2    39   BiDi ~ 0
USB-
Text GLabel 7050 2350 2    39   BiDi ~ 0
USB+
Text GLabel 7050 1900 2    39   BiDi ~ 0
1PPS
Text GLabel 10350 1450 0    39   Input ~ 0
RX
Text GLabel 10350 1350 0    39   Output ~ 0
TX
Text GLabel 10350 1550 0    39   BiDi ~ 0
1PPS
$Comp
L GND #PWR01
U 1 1 56C21FE8
P 3400 3800
F 0 "#PWR01" H 3400 3550 50  0001 C CNN
F 1 "GND" H 3400 3650 50  0000 C CNN
F 2 "" H 3400 3800 50  0000 C CNN
F 3 "" H 3400 3800 50  0000 C CNN
	1    3400 3800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 56C220C0
P 6850 3800
F 0 "#PWR02" H 6850 3550 50  0001 C CNN
F 1 "GND" H 6850 3650 50  0000 C CNN
F 2 "" H 6850 3800 50  0000 C CNN
F 3 "" H 6850 3800 50  0000 C CNN
	1    6850 3800
	1    0    0    -1  
$EndComp
Text GLabel 7050 2500 2    39   BiDi ~ 0
VDDUSB
$Comp
L C_Small C5
U 1 1 56C22406
P 2300 7150
F 0 "C5" H 2310 7220 50  0000 L CNN
F 1 "1.0 uF" H 2310 7070 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 2300 7150 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2015/08/14/S_CL10A105KQ8NNNC.pdf" H 2300 7150 50  0001 C CNN
	1    2300 7150
	1    0    0    -1  
$EndComp
$Comp
L C_Small C6
U 1 1 56C2248A
P 3150 5700
F 0 "C6" H 3160 5770 50  0000 L CNN
F 1 "0.1 uF" H 3160 5620 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3150 5700 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2015/06/15/CL10B103KO8NNNC.pdf" H 3150 5700 50  0001 C CNN
	1    3150 5700
	1    0    0    -1  
$EndComp
Text GLabel 10350 1750 0    39   Input ~ 0
GND
Text GLabel 4250 3100 0    39   BiDi ~ 0
TX2/~BOOT~
Text GLabel 4250 2950 0    39   BiDi ~ 0
RX2
$Comp
L GND #PWR03
U 1 1 56C2FF29
P 3150 6000
F 0 "#PWR03" H 3150 5750 50  0001 C CNN
F 1 "GND" H 3150 5850 50  0000 C CNN
F 2 "" H 3150 6000 50  0000 C CNN
F 3 "" H 3150 6000 50  0000 C CNN
	1    3150 6000
	1    0    0    -1  
$EndComp
$Comp
L MIC5366-3.0YC5 U1
U 1 1 56C30D16
P 3250 6950
F 0 "U1" H 3500 6700 50  0000 C CNN
F 1 "MIC5366-3.0YC5" H 3250 7150 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SC-70-5" H 3200 7250 50  0000 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/mic5365.pdf" H 3250 6950 50  0001 C CNN
	1    3250 6950
	1    0    0    -1  
$EndComp
Text GLabel 10550 2150 0    39   BiDi ~ 0
RX2
$Comp
L CP1_Small C2
U 1 1 56C33BB3
P 3000 2000
F 0 "C2" H 3010 2070 50  0000 L CNN
F 1 "2.2 uF" H 3010 1920 50  0000 L CNN
F 2 "Capacitors_SMD:c_elec_4x4.5" H 3000 2000 50  0001 C CNN
F 3 "http://media.digikey.com/pdf/Data%20Sheets/Nichicon%20PDFs/ZD_Series.pdf" H 3000 2000 50  0001 C CNN
	1    3000 2000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 56C33C5D
P 3000 2400
F 0 "#PWR04" H 3000 2150 50  0001 C CNN
F 1 "GND" H 3000 2250 50  0000 C CNN
F 2 "" H 3000 2400 50  0000 C CNN
F 3 "" H 3000 2400 50  0000 C CNN
	1    3000 2400
	1    0    0    -1  
$EndComp
$Comp
L R_Small R1
U 1 1 56C34801
P 5550 6650
F 0 "R1" V 5600 6750 50  0000 L CNN
F 1 "33 Ω" V 5600 6350 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 5550 6650 50  0001 C CNN
F 3 "http://media.digikey.com/pdf/Data%20Sheets/Samsung%20PDFs/RC_Series_ds.pdf" H 5550 6650 50  0001 C CNN
	1    5550 6650
	0    -1   -1   0   
$EndComp
$Comp
L R_Small R2
U 1 1 56C34DB8
P 5200 6950
F 0 "R2" V 5250 7050 50  0000 L CNN
F 1 "33 Ω" V 5250 6650 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 5200 6950 50  0001 C CNN
F 3 "http://media.digikey.com/pdf/Data%20Sheets/Samsung%20PDFs/RC_Series_ds.pdf" H 5200 6950 50  0001 C CNN
	1    5200 6950
	0    -1   -1   0   
$EndComp
$Comp
L C_Small C9
U 1 1 56C350B4
P 6350 7150
F 0 "C9" H 6360 7220 50  0000 L CNN
F 1 "470 pF" H 6360 7070 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6350 7150 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2015/08/14/S_CL10B471KB8NNNC.pdf" H 6350 7150 50  0001 C CNN
	1    6350 7150
	1    0    0    -1  
$EndComp
$Comp
L C_Small C8
U 1 1 56C3536C
P 5650 7150
F 0 "C8" H 5660 7220 50  0000 L CNN
F 1 "470 pF" H 5660 7070 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5650 7150 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2015/08/14/S_CL10B471KB8NNNC.pdf" H 5650 7150 50  0001 C CNN
	1    5650 7150
	1    0    0    -1  
$EndComp
Text GLabel 4100 6650 2    39   BiDi ~ 0
VDDUSB
$Comp
L C_Small C4
U 1 1 56C35A89
P 1800 7150
F 0 "C4" H 1810 7220 50  0000 L CNN
F 1 "0.1 uF" H 1810 7070 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 1800 7150 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2015/08/14/S_CL05A104KQ5NNNC.pdf" H 1800 7150 50  0001 C CNN
	1    1800 7150
	1    0    0    -1  
$EndComp
$Comp
L C_Small C1
U 1 1 56C35C41
P 900 7150
F 0 "C1" H 910 7220 50  0000 L CNN
F 1 "0.1 uF" H 910 7070 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 900 7150 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2015/08/14/S_CL05A104KQ5NNNC.pdf" H 900 7150 50  0001 C CNN
	1    900  7150
	1    0    0    -1  
$EndComp
Text GLabel 6600 6650 2    39   BiDi ~ 0
USB-
Text GLabel 6000 6950 2    39   BiDi ~ 0
USB+
$Comp
L GND #PWR05
U 1 1 56C36F74
P 5650 7450
F 0 "#PWR05" H 5650 7200 50  0001 C CNN
F 1 "GND" H 5650 7300 50  0000 C CNN
F 2 "" H 5650 7450 50  0000 C CNN
F 3 "" H 5650 7450 50  0000 C CNN
	1    5650 7450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 56C36FBC
P 6350 7450
F 0 "#PWR06" H 6350 7200 50  0001 C CNN
F 1 "GND" H 6350 7300 50  0000 C CNN
F 2 "" H 6350 7450 50  0000 C CNN
F 3 "" H 6350 7450 50  0000 C CNN
	1    6350 7450
	1    0    0    -1  
$EndComp
$Comp
L C_Small C7
U 1 1 56C373A9
P 4100 7000
F 0 "C7" H 4110 7070 50  0000 L CNN
F 1 "1.0 uF" H 4110 6920 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4100 7000 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2015/08/14/S_CL10A105KQ8NNNC.pdf" H 4100 7000 50  0001 C CNN
	1    4100 7000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 56C374D7
P 4100 7400
F 0 "#PWR07" H 4100 7150 50  0001 C CNN
F 1 "GND" H 4100 7250 50  0000 C CNN
F 2 "" H 4100 7400 50  0000 C CNN
F 3 "" H 4100 7400 50  0000 C CNN
	1    4100 7400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 56C3751B
P 2300 7400
F 0 "#PWR08" H 2300 7150 50  0001 C CNN
F 1 "GND" H 2300 7250 50  0000 C CNN
F 2 "" H 2300 7400 50  0000 C CNN
F 3 "" H 2300 7400 50  0000 C CNN
	1    2300 7400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 56C3755F
P 1800 7400
F 0 "#PWR09" H 1800 7150 50  0001 C CNN
F 1 "GND" H 1800 7250 50  0000 C CNN
F 2 "" H 1800 7400 50  0000 C CNN
F 3 "" H 1800 7400 50  0000 C CNN
	1    1800 7400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 56C375A3
P 900 7400
F 0 "#PWR010" H 900 7150 50  0001 C CNN
F 1 "GND" H 900 7250 50  0000 C CNN
F 2 "" H 900 7400 50  0000 C CNN
F 3 "" H 900 7400 50  0000 C CNN
	1    900  7400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 56C37738
P 3250 7450
F 0 "#PWR011" H 3250 7200 50  0001 C CNN
F 1 "GND" H 3250 7300 50  0000 C CNN
F 2 "" H 3250 7450 50  0000 C CNN
F 3 "" H 3250 7450 50  0000 C CNN
	1    3250 7450
	1    0    0    -1  
$EndComp
Text GLabel 900  6650 0    39   BiDi ~ 0
V_USB
Text GLabel 4900 5000 0    39   BiDi ~ 0
V_USB
NoConn ~ 6600 2050
$Comp
L INDUCTOR_SMALL FB1
U 1 1 56C39B05
P 1350 6900
F 0 "FB1" H 1350 7000 50  0000 C CNN
F 1 "BLM18HE601SN1D" H 1350 6850 50  0000 C CNN
F 2 "Capacitors_SMD:C_0603" H 1350 6900 50  0001 C CNN
F 3 "http://search.murata.co.jp/Ceramy/image/img/PDF/ENG/L0110S0101BLM18H.pdf" H 1350 6900 50  0001 C CNN
	1    1350 6900
	1    0    0    -1  
$EndComp
Text GLabel 7050 1750 2    39   BiDi ~ 0
CAN0RX
Text GLabel 7050 1600 2    39   BiDi ~ 0
CAN0TX
Text GLabel 10350 1650 0    39   BiDi ~ 0
TX2/~BOOT~
$Comp
L Battery BT1
U 1 1 56C72616
P 2100 2200
F 0 "BT1" H 2200 2250 50  0000 L CNN
F 1 "ML-1220/F1AN" H 2200 2150 50  0000 L CNN
F 2 "HEXAGON-ROBOTICS:ML-1220-F1AN" V 2100 2240 50  0001 C CNN
F 3 "https://na.industrial.panasonic.com/sites/default/pidsa/files/mlseries_datasheets_merged.pdf" V 2100 2240 50  0001 C CNN
	1    2100 2200
	1    0    0    -1  
$EndComp
$Comp
L D_Small D2
U 1 1 56C74E86
P 1250 1750
F 0 "D2" H 1200 1830 50  0000 L CNN
F 1 "DB2J314" H 1100 1670 50  0000 L CNN
F 2 "HEXAGON-ROBOTICS:DB2J314" V 1250 1750 50  0001 C CNN
F 3 "http://www.semicon.panasonic.co.jp/ds4/DB2J314_E.pdf" V 1250 1750 50  0001 C CNN
	1    1250 1750
	-1   0    0    1   
$EndComp
$Comp
L R_Small R4
U 1 1 56C750AA
P 1700 1850
F 0 "R4" H 1730 1870 50  0000 L CNN
F 1 "430" H 1730 1810 50  0000 L CNN
F 2 "Resistors_SMD:R_0402" H 1700 1850 50  0001 C CNN
F 3 "http://media.digikey.com/pdf/Data%20Sheets/Samsung%20PDFs/RC_Series_ds.pdf" H 1700 1850 50  0001 C CNN
	1    1700 1850
	1    0    0    -1  
$EndComp
Text GLabel 10550 2350 0    39   BiDi ~ 0
SCL
Text GLabel 10550 2250 0    39   BiDi ~ 0
SDA
Text GLabel 10350 1250 0    39   Input ~ 0
5V
Text GLabel 950  1750 0    39   BiDi ~ 0
VDDUSB
Text GLabel 3800 900  0    39   Input ~ 0
3.3V
Text GLabel 950  1150 0    39   Input ~ 0
3.3V
$Comp
L R_Small R3
U 1 1 56C7327E
P 1700 1250
F 0 "R3" H 1730 1270 50  0000 L CNN
F 1 "475" H 1730 1210 50  0000 L CNN
F 2 "Resistors_SMD:R_0402" H 1700 1250 50  0001 C CNN
F 3 "http://media.digikey.com/pdf/Data%20Sheets/Samsung%20PDFs/RC_Series_ds.pdf" H 1700 1250 50  0001 C CNN
	1    1700 1250
	1    0    0    -1  
$EndComp
$Comp
L D_Small D1
U 1 1 56C72C2D
P 1250 1150
F 0 "D1" H 1200 1230 50  0000 L CNN
F 1 "DB2J314" H 1100 1070 50  0000 L CNN
F 2 "HEXAGON-ROBOTICS:DB2J314" V 1250 1150 50  0001 C CNN
F 3 "http://www.semicon.panasonic.co.jp/ds4/DB2J314_E.pdf" V 1250 1150 50  0001 C CNN
	1    1250 1150
	-1   0    0    1   
$EndComp
Wire Wire Line
	1150 1750 950  1750
Connection ~ 1700 1150
Connection ~ 1700 1750
Wire Wire Line
	2700 1750 2700 1900
Connection ~ 2100 1950
Wire Wire Line
	1700 1350 2100 1350
Wire Wire Line
	2100 1350 2100 2050
Wire Wire Line
	1700 1950 2100 1950
Wire Wire Line
	1350 1150 2150 1150
Wire Wire Line
	1350 1750 2700 1750
Wire Wire Line
	1150 1150 950  1150
Connection ~ 3000 2350
Wire Wire Line
	2100 2350 3000 2350
Connection ~ 900  6900
Wire Wire Line
	1100 6900 900  6900
Connection ~ 1800 6900
Wire Wire Line
	6600 1750 7050 1750
Wire Wire Line
	7050 1600 6600 1600
Wire Wire Line
	900  6650 900  7050
Wire Wire Line
	4100 6900 3750 6900
Wire Wire Line
	4100 6900 4100 6650
Connection ~ 2300 6900
Wire Wire Line
	2300 6900 2300 7050
Wire Wire Line
	1800 6900 1800 7050
Connection ~ 2650 6900
Wire Wire Line
	2650 6900 2650 7050
Wire Wire Line
	2650 7050 2750 7050
Wire Wire Line
	1600 6900 2750 6900
Wire Wire Line
	3250 7450 3250 7350
Wire Wire Line
	900  7400 900  7250
Wire Wire Line
	1800 7250 1800 7400
Wire Wire Line
	2300 7400 2300 7250
Wire Wire Line
	4100 7100 4100 7400
Wire Wire Line
	5650 7050 5650 6950
Wire Wire Line
	6350 6650 6350 7050
Wire Wire Line
	5650 7450 5650 7250
Wire Wire Line
	6350 7250 6350 7450
Connection ~ 5650 6950
Wire Wire Line
	5300 6950 6000 6950
Wire Wire Line
	4900 6950 5100 6950
Wire Wire Line
	5450 6650 5250 6650
Wire Wire Line
	5650 6650 6600 6650
Wire Wire Line
	6600 2350 7050 2350
Wire Wire Line
	7050 2200 6600 2200
Wire Wire Line
	7050 2500 6600 2500
Connection ~ 3000 1900
Wire Wire Line
	2700 1900 4500 1900
Wire Wire Line
	3000 2100 3000 2400
Connection ~ 4250 900 
Wire Wire Line
	7800 2800 6600 2800
Wire Wire Line
	7800 900  7800 2800
Connection ~ 3150 5500
Wire Wire Line
	3150 5500 3150 5600
Wire Wire Line
	4250 1750 4250 900 
Wire Wire Line
	4500 1750 4250 1750
Wire Wire Line
	3150 6000 3150 5800
Wire Wire Line
	4500 2950 4250 2950
Wire Wire Line
	7050 1900 6600 1900
Wire Wire Line
	10350 1750 10550 1750
Wire Wire Line
	4500 3100 4250 3100
Wire Wire Line
	10550 1250 10350 1250
Wire Wire Line
	10350 1350 10550 1350
Connection ~ 6850 3250
Wire Wire Line
	6600 2950 6850 2950
Wire Wire Line
	6850 3250 6600 3250
Wire Wire Line
	6850 2950 6850 3800
Connection ~ 3400 3250
Wire Wire Line
	4500 1600 3400 1600
Wire Wire Line
	3400 1600 3400 3800
Wire Wire Line
	4500 3250 3400 3250
Wire Wire Line
	4250 2050 4500 2050
Wire Wire Line
	4500 2200 4250 2200
Wire Wire Line
	4250 2500 4500 2500
Wire Wire Line
	4500 2350 4250 2350
Wire Wire Line
	10550 1650 10350 1650
Wire Wire Line
	10350 1550 10550 1550
Wire Wire Line
	10550 1450 10350 1450
Wire Wire Line
	8000 3100 6600 3100
Wire Wire Line
	2150 1150 2150 1700
Wire Wire Line
	2150 1700 2750 1700
Wire Wire Line
	2750 1700 2750 1850
Wire Wire Line
	2750 1850 3000 1850
Wire Wire Line
	3000 1850 3000 1900
$Comp
L AP7313-33SAG-7 U3
U 1 1 56C7B5B8
P 2050 5550
F 0 "U3" H 2300 5400 50  0000 C CNN
F 1 "AP7313-33SAG-7" H 2050 5750 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 1950 5300 50  0001 C CNN
F 3 "http://www.diodes.com/_files/datasheets/AP7313.pdf" H 2050 5550 50  0001 C CNN
	1    2050 5550
	1    0    0    -1  
$EndComp
Text GLabel 800  5500 0    39   Input ~ 0
5V
Text GLabel 3350 5500 2    39   Input ~ 0
3.3V
$Comp
L GND #PWR012
U 1 1 56C7C35D
P 2050 6100
F 0 "#PWR012" H 2050 5850 50  0001 C CNN
F 1 "GND" H 2050 5950 50  0000 C CNN
F 2 "" H 2050 6100 50  0000 C CNN
F 3 "" H 2050 6100 50  0000 C CNN
	1    2050 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 6100 2050 5950
$Comp
L C_Small C10
U 1 1 56C7C5D2
P 1150 5700
F 0 "C10" H 1160 5770 50  0000 L CNN
F 1 "1.0 uF" H 1160 5620 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 1150 5700 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2015/08/14/S_CL10A105KQ8NNNC.pdf" H 1150 5700 50  0001 C CNN
	1    1150 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  5500 1550 5500
Wire Wire Line
	1150 5500 1150 5600
Connection ~ 1150 5500
Wire Wire Line
	2550 5500 3350 5500
Wire Wire Line
	2750 5500 2750 5600
Connection ~ 2750 5500
$Comp
L GND #PWR013
U 1 1 56C7CB47
P 2750 6100
F 0 "#PWR013" H 2750 5850 50  0001 C CNN
F 1 "GND" H 2750 5950 50  0000 C CNN
F 2 "" H 2750 6100 50  0000 C CNN
F 3 "" H 2750 6100 50  0000 C CNN
	1    2750 6100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 56C7CB9F
P 1150 6100
F 0 "#PWR014" H 1150 5850 50  0001 C CNN
F 1 "GND" H 1150 5950 50  0000 C CNN
F 2 "" H 1150 6100 50  0000 C CNN
F 3 "" H 1150 6100 50  0000 C CNN
	1    1150 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 6100 2750 5800
Wire Wire Line
	1150 5800 1150 6100
$Comp
L Led_Small D3
U 1 1 56C7D65E
P 9950 5750
F 0 "D3" H 9900 5875 50  0000 L CNN
F 1 "POWER" H 9775 5650 50  0000 L CNN
F 2 "LEDs:LED-0603" V 9950 5750 50  0001 C CNN
F 3 "http://optoelectronics.liteon.com/upload/download/DS22-2005-077/S_110_LTST-C193KRKT-5A.pdf" V 9950 5750 50  0001 C CNN
	1    9950 5750
	1    0    0    -1  
$EndComp
$Comp
L Led_Small D4
U 1 1 56C7D6C1
P 9950 6150
F 0 "D4" H 9900 6275 50  0000 L CNN
F 1 "GPS" H 9775 6050 50  0000 L CNN
F 2 "LEDs:LED-0603" V 9950 6150 50  0001 C CNN
F 3 "http://optoelectronics.liteon.com/upload/download/DS22-2004-060/P_100_LTST-C193TBKT-5A.pdf" V 9950 6150 50  0001 C CNN
	1    9950 6150
	1    0    0    -1  
$EndComp
Text GLabel 10700 5750 2    39   Input ~ 0
5V
$Comp
L R_Small R5
U 1 1 56C7E35C
P 10400 5750
F 0 "R5" V 10350 5700 50  0000 L CNN
F 1 "150-1/8W" V 10500 5550 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 10400 5750 50  0001 C CNN
F 3 "http://media.digikey.com/pdf/Data%20Sheets/Samsung%20PDFs/RC_Series_ds.pdf" H 10400 5750 50  0001 C CNN
	1    10400 5750
	0    1    1    0   
$EndComp
$Comp
L R_Small R6
U 1 1 56C7E3F0
P 10400 6150
F 0 "R6" V 10300 6100 50  0000 L CNN
F 1 "27-1/8W" V 10500 6000 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 10400 6150 50  0001 C CNN
F 3 "http://media.digikey.com/pdf/Data%20Sheets/Samsung%20PDFs/RC_Series_ds.pdf" H 10400 6150 50  0001 C CNN
	1    10400 6150
	0    1    1    0   
$EndComp
$Comp
L GND #PWR015
U 1 1 56C7E5FF
P 9450 5750
F 0 "#PWR015" H 9450 5500 50  0001 C CNN
F 1 "GND" H 9450 5600 50  0000 C CNN
F 2 "" H 9450 5750 50  0000 C CNN
F 3 "" H 9450 5750 50  0000 C CNN
	1    9450 5750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 56C7E683
P 9450 6150
F 0 "#PWR016" H 9450 5900 50  0001 C CNN
F 1 "GND" H 9450 6000 50  0000 C CNN
F 2 "" H 9450 6150 50  0000 C CNN
F 3 "" H 9450 6150 50  0000 C CNN
	1    9450 6150
	1    0    0    -1  
$EndComp
Text GLabel 10700 6150 2    39   Output ~ 0
TX
Wire Wire Line
	10700 5750 10500 5750
Wire Wire Line
	10300 5750 10050 5750
Wire Wire Line
	9850 5750 9450 5750
Wire Wire Line
	9450 6150 9850 6150
Wire Wire Line
	10050 6150 10300 6150
Wire Wire Line
	10500 6150 10700 6150
Text GLabel 10550 2550 0    39   BiDi ~ 0
CAN0RX
Text GLabel 10550 2450 0    39   BiDi ~ 0
CAN0TX
Wire Notes Line
	550  5200 3600 5200
Wire Notes Line
	3600 5200 3600 6400
Wire Notes Line
	3600 6400 550  6400
Wire Notes Line
	550  6400 550  5200
Wire Notes Line
	6950 4450 6950 7650
Wire Notes Line
	6950 7650 4550 7650
Wire Notes Line
	4550 7650 4550 5800
Wire Notes Line
	9250 5550 10950 5550
Wire Notes Line
	10950 5550 10950 6350
Wire Notes Line
	10950 6350 9250 6350
Wire Notes Line
	9250 6350 9250 5550
Wire Notes Line
	9900 1050 10950 1050
Wire Notes Line
	10950 1050 10950 1850
Wire Notes Line
	10950 1850 9900 1850
Wire Notes Line
	9900 1850 9900 1050
Wire Notes Line
	10150 1950 10150 2750
Wire Notes Line
	10150 2750 10950 2750
Wire Notes Line
	10950 2750 10950 1950
Wire Notes Line
	10950 1950 10150 1950
Wire Wire Line
	3800 900  7800 900 
$Comp
L CP_Small C11
U 1 1 56C8DB6D
P 2750 5700
F 0 "C11" H 2760 5770 50  0000 L CNN
F 1 "10.0 uF" H 2760 5620 50  0000 L CNN
F 2 "Capacitors_SMD:c_elec_5x5.3" H 2750 5700 50  0001 C CNN
F 3 "http://media.digikey.com/pdf/Data%20Sheets/Nichicon%20PDFs/ZD_Series.pdf" H 2750 5700 50  0001 C CNN
	1    2750 5700
	1    0    0    -1  
$EndComp
Text Notes 2750 5450 0    39   ~ 0
Closed to SL869DR
Wire Notes Line
	550  6500 550  7650
Wire Notes Line
	550  7650 4500 7650
Wire Notes Line
	4500 7650 4500 6500
Wire Notes Line
	4500 6500 550  6500
$Comp
L USB_TYPE_C P12
U 1 1 56D56704
P 5800 5250
F 0 "P12" H 5800 5250 60  0000 C CNN
F 1 "USB_TYPE_C" H 5800 5500 60  0000 C CNN
F 2 "HEXAGON-ROBOTICS:JAE-DX07S024JJ3R1300" H 5800 5250 60  0001 C CNN
F 3 "http://www.jae.com/z-en/pdf/MB-0297-1E_DX07.pdf" H 5800 5250 60  0001 C CNN
	1    5800 5250
	1    0    0    -1  
$EndComp
Wire Notes Line
	6950 4450 4550 4450
Wire Notes Line
	4550 4450 4550 5850
Text GLabel 6650 5000 2    39   BiDi ~ 0
V_USB
Text GLabel 5250 6650 0    39   BiDi ~ 0
minus
Text GLabel 4900 6950 0    39   BiDi ~ 0
PLUS
Text GLabel 4900 5300 0    39   BiDi ~ 0
minus
Text GLabel 4900 5200 0    39   BiDi ~ 0
PLUS
Text GLabel 6650 5200 2    39   BiDi ~ 0
minus
Text GLabel 6650 5300 2    39   BiDi ~ 0
PLUS
NoConn ~ 6650 5700
NoConn ~ 6650 5600
Text GLabel 6650 5500 2    39   BiDi ~ 0
V_USB
Text GLabel 4900 5500 0    39   BiDi ~ 0
V_USB
$Comp
L GND #PWR017
U 1 1 56D591AE
P 5800 6250
F 0 "#PWR017" H 5800 6000 50  0001 C CNN
F 1 "GND" H 5800 6100 50  0000 C CNN
F 2 "" H 5800 6250 50  0000 C CNN
F 3 "" H 5800 6250 50  0000 C CNN
	1    5800 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 6250 6650 6250
Wire Wire Line
	6650 6250 6650 5800
Wire Wire Line
	5800 6150 5800 6250
Wire Wire Line
	4900 6250 4900 5800
Connection ~ 5800 6250
NoConn ~ 6650 4800
NoConn ~ 6650 4900
NoConn ~ 4900 4900
NoConn ~ 4900 4800
$Comp
L GND #PWR018
U 1 1 56D59768
P 6800 4700
F 0 "#PWR018" H 6800 4450 50  0001 C CNN
F 1 "GND" H 6800 4550 50  0000 C CNN
F 2 "" H 6800 4700 50  0000 C CNN
F 3 "" H 6800 4700 50  0000 C CNN
	1    6800 4700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR019
U 1 1 56D597F7
P 4750 4700
F 0 "#PWR019" H 4750 4450 50  0001 C CNN
F 1 "GND" H 4750 4550 50  0000 C CNN
F 2 "" H 4750 4700 50  0000 C CNN
F 3 "" H 4750 4700 50  0000 C CNN
	1    4750 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 4700 6650 4700
Wire Wire Line
	4900 4700 4750 4700
NoConn ~ 6650 5100
NoConn ~ 6650 5400
NoConn ~ 4900 5400
NoConn ~ 4900 5100
NoConn ~ 4900 5600
NoConn ~ 4900 5700
Connection ~ 6350 6650
$Comp
L CONN_01X05 P2
U 1 1 56D5EEB5
P 10750 2350
F 0 "P2" H 10750 2650 50  0000 C CNN
F 1 "EXTRAS" V 10850 2350 50  0000 C CNN
F 2 "HEXAGON-ROBOTICS:SM05B-GHS-TB" H 10750 2350 50  0001 C CNN
F 3 "http://www.jst-mfg.com/product/pdf/eng/eGH.pdf" H 10750 2350 50  0001 C CNN
	1    10750 2350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
