# -*- coding: utf8 -*-
#!/usr/bin/python
#
# This is derived from a cadquery script for generating QFP/GullWings models in X3D format.
#
# from https://bitbucket.org/hyOzd/freecad-macros
# author hyOzd
#
# Dimensions are from Jedec MS-026D document.

## file of parametric definitions

from collections import namedtuple

destination_dir="/generated_cap/"
# destination_dir="./"

# body color
base_color = (39,39,39)
body_color = (220,220,220)
mark_color = (39,39,39)
pins_color = (240, 240, 240)


### Parametric Values
##
Params = namedtuple("Params", [
    'L',   # overall height
    'D',   # diameter
    'A',   # base width (x&y)
    'H',   # max width (x) with pins
    'P',   # distance between pins
    'W',   # pin width
    'modelName', # modelName
    'rotation',  # rotation if required
    'dest_dir_prefix' #destination dir prefix
])

all_params_radial_smd_cap = {# Aluminum SMD capacitors
        # Dimensions per http://media.digikey.com/pdf/Data%20Sheets/Nichicon%20PDFs/ZD_Series.pdf
    
    "UZD1E2R2MCL1GB" : Params( # note L model height
        L         =3.2,    # overall height
        D         =4.0,     # diameter
        A         =4.3,    # base width (x&y)
        H         =4.6,     # max width (x) with pins
        P         =1.0,     # distance between pins
        W         =0.65,     # pin width
        modelName = 'c_el_UZD1E2R2MCL1GB', #modelName
        rotation  = -90, # rotation if required
        dest_dir_prefix = 'cap_DR8'
    ),
    "UZD1C100MCL1GB" : Params( # note L model height
        L         =3.2,    # overall height
        D         =5.0,     # diameter
        A         =5.3,    # base width (x&y)
        H         =5.5,     # max width (x) with pins
        P         =1.3,     # distance between pins
        W         =0.65,     # pin width
        modelName = 'c_el_UZD1C100MCL1GB', #modelName
        rotation  = -90, # rotation if required
        dest_dir_prefix = 'cap_DR8'
    ),
}